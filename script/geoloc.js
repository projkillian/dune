function isSupported(){
    return "geolocation" in navigator;
}

function getPosition(options) {
    return new Promise(function (resolve, reject) {
      navigator.geolocation.getCurrentPosition(resolve, reject, options);
    });
}