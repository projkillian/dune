let crewContainer = document.querySelector(".crew-cards-container");
let detailedContainer = document.querySelector(".detail-crew-container")
let defaultImage = "/c1AiZTXyyzmPOlTLSubp7CEeYj.jpg";

async function getPersonImage(imageURL) {
    try {
        return await getData("https://image.tmdb.org/t/p/w500/" + imageURL);
    }
    catch(err) {
        console.log(err);
    }
}
async function getMovieCredits(movieId){
    try {
        return await getData("https://api.themoviedb.org/3/movie/" + movieId + "/credits?api_key=8511c090b22f5239695feba184e0fff3");
    }
    catch(err) {
        console.log(err);
    }
}
async function getPersonDetail(personId) {
    try {
        return await getData("https://api.themoviedb.org/3/person/" + personId + "?api_key=8511c090b22f5239695feba184e0fff3");
    }
    catch(err) {
        console.log(err);
    }
}

async function getData(ajaxUrl){
    return $.ajax({
        type: "GET",
        url: ajaxUrl,
        dataType: 'json',
    });
}


getMovieCredits("438631").then(
    response => {
        let members = response['cast'].concat(response['crew']);
        members.forEach(castMember => {
            let castMemberImage = document.createElement('img');
            castMember["profile_path"] === null ? castMemberImage.src = "https://image.tmdb.org/t/p/w500" + defaultImage : castMemberImage.src = "https://image.tmdb.org/t/p/w500" + castMember["profile_path"]
            let castMemberTitle = document.createElement("h3");
            castMemberTitle.textContent = castMember["name"];
            let castMemberFilterDiv = document.createElement('div');
            castMemberFilterDiv.classList.add('filter');
            let castMemberCard = document.createElement('div');
            castMemberCard.classList.add('crew-card');
            castMemberCard.id = castMember['id'];
            castMemberCard.appendChild(castMemberImage);
            castMemberCard.appendChild(castMemberFilterDiv);
            castMemberCard.appendChild(castMemberTitle);
            crewContainer.appendChild(castMemberCard);
        })
            let DOMmembers = document.querySelectorAll(".crew-card");
            DOMmembers.forEach(member => {
                member.addEventListener("click", () => {
                    showDetailedPerson(member.id);
                })
            })
    });

function showDetailedPerson(id){
    getPersonDetail(id).then(
        response => {
            let image = document.querySelector(".detailed-crew-image");
            let name = document.querySelector(".detailed-crew-name");
            let birthDate = document.querySelector(".detailed-crew-birthDate");
            let birthPlace = document.querySelector(".detailed-crew-birthPlace");
            let description = document.querySelector(".detailed-crew-description");
            response['profile_path'] === null ? image.src = "https://image.tmdb.org/t/p/w500" + defaultImage : image.src = "https://image.tmdb.org/t/p/w500" + response['profile_path'];
            name.textContent = response["name"];
            birthDate.textContent = response["birthday"];
            birthPlace.textContent = response["place_of_birth"];
            description.textContent = response["biography"];
        });
}
