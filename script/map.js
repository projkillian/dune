let carte;
if (isSupported()){
  getPosition()
      .then((position) => {
        carte = L.map('map', {
          center: [position.coords.latitude, position.coords.longitude],
          zoom: 15,
          minZoom: 4,
          maxZoom: 19,
        });
        initMap()
      })
      .catch((err) => {
        console.error(err.message);
      });

}
else {
  carte = L.map('map', {
    center: [47.2608333, 2.4188888888888886],
    zoom: 5,
    minZoom: 4,
    maxZoom: 19,
  });
  initMap()
}

function initMap(){
  // Création de la carte, vide à ce stade

  // Ajout des tuiles (ici OpenStreetMap)
  // https://wiki.openstreetmap.org/wiki/Tiles#Servers
  L.tileLayer('https://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>',
  }).addTo(carte);

  // Ajout de l'échelle
  L.control.scale().addTo(carte);

  try {
    fetch('http://localhost:80/Dune/script/cinema.geojson')
    .then((res) => {
        return res.json()
    })
    .then((data) => {
      console.log(data)
      data.features.forEach(feature => {
        L.geoJSON(feature).addTo(carte);
      });
    });

  }
  catch(e){
    console.log(e);
  }
}
