const sections = document.querySelectorAll("section");
const previewTitles = document.querySelectorAll(".p-title");
const backBtn = document.querySelector(".back");
let isOnVideoSlide = false;
let index = 0

sections.forEach(section => {
    section.addEventListener("mouseenter", () => {
        clearInterval(carrouselInterval);
        sections.forEach((e, index) => {
            if (section === e){
                e.classList.add("focused");
                e.addEventListener("click", () => openSection(e));
                previewTitles.forEach((pt, ptIndex) => {
                    pt.classList.remove("active");
                    if (ptIndex === index){
                        pt.classList.add("active")
                    }
                })
            }
            else {
                e.classList.remove("focused");
            }
        })
    })
});

backBtn.addEventListener("click", () => back());

function openSection(section) {
    sections.forEach(s => {
        s.classList.add("closed")
    })
    section.classList.remove("closed");
    section.classList.add("opened")
    setTimeout(() => {
        section.querySelector(".content").classList.add("active")
    }, 600)
    backBtn.classList.add("active")
    section.classList.contains("home") ? isOnVideoSlide = true : isOnVideoSlide = false;
    triggerVideoThumbnail(isOnVideoSlide);
}

function back() {
    sections.forEach(s => {
        s.classList.remove("opened");
        s.classList.remove("closed");
        s.querySelector(".content").classList.remove("active");
    })
    backBtn.classList.remove("active")
    triggerVideoThumbnail(false)
}

function toggleSlide(){
    sections.forEach(e => {
        e.classList.remove("focused");
    })
    sections[index].classList.add("focused");
    previewTitles.forEach((pt, ptIndex) => {
        pt.classList.remove("active");
        if (ptIndex === index){
            pt.classList.add("active")
        }
    })
    index === 5 ? index = 0 : index++
}
toggleSlide();
const carrouselInterval = setInterval(toggleSlide, 5000);

/* CARROUSEL */

const arrowLeft = document.querySelector(".arrow-left");
const arrowRight = document.querySelector(".arrow-right");
const carrouselImages = document.querySelectorAll(".characters img");
const nameTitle = document.querySelector(".characters h3")
const names = [
    "Timothée Chalamet: Paul Atreïdes",
    "Rebecca Ferguson: Lady Jessica",
    "Oscar Isaac: Duke Leto Atreïdes",
    "Jason Momoa: Duncan Idaho",
    "Josh Brolin: Gurney Halleck",
    "Chang Chen: Dr. Wellington Yueh",
    "Zendaya: Chani",
    "Javier Bardem: Stilgar",
    "Sharon Duncan-Brewster: Liet Kynes",
    "Stellan Skarsgard: Baron Harkonnen",
    "Dave Bautista: Rabban Harkonnen"];
let carrouselIndex = 0;

arrowRight.addEventListener("click", function() {nextSlide()});
arrowLeft.addEventListener("click", function() {prevSlide()});


function nextSlide(){
    carrouselImages[carrouselIndex].classList.remove("active");
    carrouselIndex === 10 ? carrouselIndex = 0 : carrouselIndex++
    carrouselImages[carrouselIndex].classList.add("active");
    nameTitle.textContent = names[carrouselIndex];
}

function prevSlide(){
    carrouselImages[carrouselIndex].classList.remove("active");
    carrouselIndex === 0 ? carrouselIndex = 10 : carrouselIndex--
    carrouselImages[carrouselIndex].classList.add("active");
    nameTitle.textContent = names[carrouselIndex];
}

/* IMAGES & VIDEOS */
let carrouImgVidIndex = 0;
const spans = document.querySelectorAll(".video-images span")
// Gestion des événements
$('.video-images span').click(function () {
  // Récupération index
  let indexN = $('span').index(this);
  spans.forEach(span => {
    span.classList.remove("active")
  })
  spans[indexN].classList.add("active");

  showImg()

  // Mettre à jour l'index
  carrouImgVidIndex = indexN;
});

$('.video-images .arrow-right').click(function () {
    let tempIndex = carrouImgVidIndex;
    carrouImgVidIndex === 9 ? carrouImgVidIndex = 0 : carrouImgVidIndex++;
    showImg(tempIndex)
    spans.forEach(span => {
        span.classList.remove("active")
      })
      spans[carrouImgVidIndex].classList.add("active");
})
$('.video-images .arrow-left').click(function () {
    let tempIndex = carrouImgVidIndex;
    carrouImgVidIndex === 0 ? carrouImgVidIndex = 9 : carrouImgVidIndex--;
    showImg(tempIndex)
    spans.forEach(span => {
        span.classList.remove("active")
      })
      spans[carrouImgVidIndex].classList.add("active");
})

function showImg(tempIndex){
    $('.video-images img').eq(tempIndex).fadeOut(1000).end().eq(carrouImgVidIndex).fadeIn(1000);
}

/* PHONE BURGER MENU */
const burgerMenu = document.querySelector(".burger");
const navMenu = document.querySelector(".header-right-bar");

burgerMenu.addEventListener("click", () => {
   burgerMenu.classList.toggle("active");
   navMenu.classList.toggle("active");
});


