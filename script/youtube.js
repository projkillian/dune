// Obligatoire pour Codesandbox (pb de dépendances...)
// Le fichier script.js est donc ici inutile...
// Le script est placé à la fin du body car l'attribut defer ne fonctionne que pour des scripts externes
// CE N'EST PAS UNE BONNE PRATIQUE !!!

// Variable globale contenant l'état du lecteur
let etatLecteur;

function lecteurPret(event) {
  // event.target = lecteur
  event.target.setVolume(50);
}

function changementLecteur(event) {
  // event.data = état du lecteur
  etatLecteur = event.data;
}

let lecteur;

function onYouTubeIframeAPIReady() {
  lecteur = new YT.Player("video", {
    height: "390",
    width: "640",
    videoId: "n9xhJrPXop4",
    playerVars: {
      color: "white",
      enablejsapi: 1,
      modestbranding: 1,
      rel: 0
    },
    events: {
      onReady: lecteurPret,
      onStateChange: changementLecteur
    }
  });
}

// Hauteur de la vidéo
const hauteurVideo = $("#video").height();
// Position Y de la vidéo
const posYVideo = $("#video").offset().top;
// Valeur declenchant la modification de l'affichage (choix "esthétique")
const seuil = posYVideo + 0.75 * hauteurVideo;


function triggerVideoThumbnail(isOnVideoSlide) {
  let video = document.querySelector(".home-video");
  let videoContainer = document.querySelector(".home .content");
  if (isOnVideoSlide === false && etatLecteur === YT.PlayerState.PLAYING){
    video.classList.add("thumbnail")
    videoContainer.style.opacity = 1;
  }
  else if (isOnVideoSlide === true) {
    video.classList.remove("thumbnail")
  }
  else {
    videoContainer.style.opacity = 0;
  }

}



